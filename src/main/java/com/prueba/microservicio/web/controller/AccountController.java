package com.prueba.microservicio.web.controller;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.domain.service.AccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/cuentas")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/all")
    @ApiOperation("Obtener todas las cuentas")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Account>> getAll(){
        return new ResponseEntity<>(accountService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Obtener cuentas por id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Account not found"),
    })
    public ResponseEntity<Account> getAccount(@ApiParam(value = "The id of the account", required = true, example = "2")
                                            @PathVariable("id") int accountId) {
        return accountService.getAccount(accountId)
                .map(account -> new ResponseEntity<>(account, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/save")
    @ApiOperation("Crear Cuenta")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<Account> save(@RequestBody Account account) {
        return new ResponseEntity<>(accountService.save(account), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    @ApiOperation("Actualizar Cuenta")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<Account> update(@RequestBody Account account) {
        return new ResponseEntity<>(accountService.update(account), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        if (accountService.delete(id)) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/reportes/{idClient}/{startDate}/{endDate}")
    @ApiOperation("Obtener cuentas y movimientos por fecha")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Report Not Found"),
    })
    public ResponseEntity<List<Account>> getByPKClientBetweenDate(
            @ApiParam(value = "id of client", required = true, example = "2")
            @PathVariable("idClient")  int idClient,
            @ApiParam(value = "The start of the period", required = true, example = "2022-03-01")
            @PathVariable("startDate")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @ApiParam(value = "The end of the period", required = true, example = "2022-03-31")
            @PathVariable("endDate")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)   LocalDateTime endDate)
    {
        return new ResponseEntity<>(accountService.getByPKClientBetweenDate(idClient,startDate,endDate), HttpStatus.OK);
    }
}
