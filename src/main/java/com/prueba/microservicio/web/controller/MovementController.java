package com.prueba.microservicio.web.controller;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.domain.service.AccountService;
import com.prueba.microservicio.domain.service.MovementService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movimientos")
public class MovementController {
    @Autowired
    private MovementService movementService;

    @GetMapping("/all")
    @ApiOperation("Obtener todos los movimientos")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<List<Movement>> getAll(){
        return new ResponseEntity<>(movementService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation("Obtener movimiento por id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Account not found"),
    })
    public ResponseEntity<Movement> getAccount(@ApiParam(value = "The id of the account", required = true, example = "2")
                                              @PathVariable("id") int id) {
        return movementService.getMovement(id)
                .map(movement -> new ResponseEntity<>(movement, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @PostMapping("/save")
    @ApiOperation("Crear Cuenta")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<Movement> save(@RequestBody Movement movement) {
        return (null==movementService.save(movement))?
                new ResponseEntity<>(movement, HttpStatus.NOT_ACCEPTABLE):
                new ResponseEntity<>(movement, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    @ApiOperation("Actualizar Cuenta")
    @ApiResponse(code = 200, message = "OK")
    public ResponseEntity<Movement> update(@RequestBody Movement movement) {
        return new ResponseEntity<>(movementService.update(movement), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") int id) {
        if (movementService.delete(id)) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
