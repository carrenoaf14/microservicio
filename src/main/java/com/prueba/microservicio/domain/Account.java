package com.prueba.microservicio.domain;


import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Data
public class Account {
    private Integer accountPK;
    private Integer clientPK;
    private BigDecimal initialBalance;
    private BigInteger accountNumber;
    private List<Movement> movements;
}
