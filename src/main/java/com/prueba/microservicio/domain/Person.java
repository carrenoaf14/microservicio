package com.prueba.microservicio.domain;

import lombok.Data;

import java.math.BigInteger;

@Data
public class Person {
    private String name;
    private String gender;
    private String address;
    private BigInteger phoneNumber;
    private BigInteger identification;
}
