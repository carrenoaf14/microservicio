package com.prueba.microservicio.domain.service;

import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.domain.repository.MovementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovementService {
    @Autowired
    private MovementRepository movementRepository;
    @Autowired
    private AccountService accountService;

    public List<Movement> getAll(){ return movementRepository.getAll();}
    public Movement save(Movement movement){
        return accountService.getAccount(movement.getAccountPK()).map(account -> {
            if("Debito".equals(movement.getMovementType()) &&
                    movement.getValue().compareTo(account.getInitialBalance()) < 0){
                        movement.setBalance(
                                account.getInitialBalance().subtract(movement.getValue())
                        );
                        account.setInitialBalance(
                                account.getInitialBalance().subtract(movement.getValue())
                        );
                        accountService.save(account);
                        return movementRepository.save(movement);
            }else if("Credito".equals(movement.getMovementType())) {
                movement.setBalance(
                        account.getInitialBalance().add(movement.getValue())
                );
                account.setInitialBalance(
                        account.getInitialBalance().add(movement.getValue())
                );
                accountService.save(account);
                return movementRepository.save(movement);
            }else if(movement.getValue().compareTo(account.getInitialBalance()) == 0){
                //Saldo igual a Cero
                return null;
            }else{
                return null;
            }
        }).orElse(null);
    }
    public Movement update(Movement movement){
        return getMovement(movement.getMovementPK()).map(mov ->  movementRepository.save(movement)
        ).orElse(null);
    }
    public Optional<Movement> getMovement(int movementId) {return movementRepository.getMovement(movementId);}

    public boolean delete(int id) {
        return getMovement(id).map(product -> {
            movementRepository.delete(id);
            return true;
        }).orElse(false);
    }

}
