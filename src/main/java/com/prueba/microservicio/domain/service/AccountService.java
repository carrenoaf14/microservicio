package com.prueba.microservicio.domain.service;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.repository.AccountRepository;
import com.prueba.microservicio.domain.repository.ClientRepository;
import com.prueba.microservicio.domain.repository.MovementRepository;
import com.prueba.microservicio.persistence.entity.Cuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private MovementRepository movementRepository;

    public List<Account> getAll(){ return accountRepository.getAll();}
    public Account save(Account account){ return accountRepository.save(account);}
    public Account update(Account account){
        return getAccount(account.getAccountPK()).map(cli -> {
            return accountRepository.save(account);
        }).orElse(null);
    }
    public Optional<Account> getAccount(int accountId) {return accountRepository.getAccount(accountId);}

    public boolean delete(int id) {
        return getAccount(id).map(product -> {
            accountRepository.delete(id);
            return true;
        }).orElse(false);
    }
    public List<Account> getByPKClientBetweenDate(int idClient, LocalDateTime startDate, LocalDateTime endDate){
        System.out.printf(startDate.toString() + endDate.toString());
        return accountRepository.getByPKClientBetweenDate(idClient,startDate,endDate);
    }
}
