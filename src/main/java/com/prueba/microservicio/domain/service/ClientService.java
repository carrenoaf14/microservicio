package com.prueba.microservicio.domain.service;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;
    public List<Client> getAll(){ return clientRepository.getAll();}
    public Client save(Client client){ return clientRepository.save(client);}
    public Client update(Client client){
        return getClient(client.getClientpk()).map(cli -> {
            return clientRepository.save(client);
        }).orElse(null);
    }
    public Optional<Client> getClient(int clientId) {return clientRepository.getClient(clientId);}

    public boolean delete(int id) {
        return getClient(id).map(product -> {
            clientRepository.delete(id);
            return true;
        }).orElse(false);
    }

}

