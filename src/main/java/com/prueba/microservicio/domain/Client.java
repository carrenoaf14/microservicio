package com.prueba.microservicio.domain;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
public class Client {
    private Boolean state;
    private Integer clientpk;
    private String name;
    private BigInteger identification;
    private List<Account> accounts;
}
