package com.prueba.microservicio.domain.repository;

import com.prueba.microservicio.domain.Client;

import java.util.List;
import java.util.Optional;

public interface ClientRepository {
    List<Client> getAll();
    Client save(Client client);
    Optional<Client> getClient(int id);
    void delete(int id);

}
