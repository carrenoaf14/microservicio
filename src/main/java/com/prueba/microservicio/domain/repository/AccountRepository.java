package com.prueba.microservicio.domain.repository;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.domain.Client;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    List<Account> getAll();
    Account save(Account account);
    Optional<Account> getAccount(int id);
    void delete(int id);
    List<Account> getByPKClientBetweenDate(int id, LocalDateTime startDate, LocalDateTime endDate);
}
