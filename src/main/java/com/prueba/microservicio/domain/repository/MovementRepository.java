package com.prueba.microservicio.domain.repository;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.persistence.entity.Movimiento;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MovementRepository {

    List<Movement> getAll();
    Movement save(Movement movement);
    Optional<Movement> getMovement(int id);
    void delete(int id);
    List<Movement> findByFechaBetween(LocalDateTime fechaInicio, LocalDateTime fechaFin);
}
