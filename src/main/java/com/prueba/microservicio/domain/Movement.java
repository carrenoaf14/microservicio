package com.prueba.microservicio.domain;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Movement {
    private BigDecimal balance;
    private BigDecimal value;
    private Integer movementPK;
    private Integer accountPK;
    private String movementType;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime date;
}
