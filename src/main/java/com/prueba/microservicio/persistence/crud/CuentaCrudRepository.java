package com.prueba.microservicio.persistence.crud;

import com.prueba.microservicio.persistence.entity.Cuenta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CuentaCrudRepository extends CrudRepository<Cuenta, Integer> {

    List<Cuenta> findByPKCliente(int PKCliente);
}
