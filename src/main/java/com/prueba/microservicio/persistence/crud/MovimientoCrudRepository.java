package com.prueba.microservicio.persistence.crud;

import com.prueba.microservicio.persistence.entity.Movimiento;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface MovimientoCrudRepository extends CrudRepository<Movimiento, Integer> {
    List<Movimiento> findByFechaBetween(LocalDateTime fechaInicio, LocalDateTime fechaFin);

}
