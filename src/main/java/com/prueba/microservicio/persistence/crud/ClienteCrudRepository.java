package com.prueba.microservicio.persistence.crud;

import com.prueba.microservicio.persistence.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteCrudRepository extends CrudRepository<Cliente, Integer> {

}
