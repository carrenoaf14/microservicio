package com.prueba.microservicio.persistence;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.repository.ClientRepository;
import com.prueba.microservicio.persistence.crud.ClienteCrudRepository;
import com.prueba.microservicio.persistence.entity.Cliente;
import com.prueba.microservicio.persistence.mapper.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ClienteRepository implements ClientRepository {

    @Autowired
    private ClienteCrudRepository clienteCrudRepository;

    @Autowired
    private ClientMapper clientMapper;

    @Override
    public List<Client> getAll(){
        List<Cliente> clientes = (List<Cliente>) clienteCrudRepository.findAll();
        return clientMapper.toClients(clientes);
    }

    @Override
    public Client save(Client client) {
        Cliente cliente = clientMapper.toCliente(client);
        return clientMapper.toClient(clienteCrudRepository.save(cliente));
    }

    @Override
    public Optional<Client> getClient(int id) {
        return clienteCrudRepository.findById(id).map(cliente -> clientMapper.toClient(cliente));
    }

    @Override
    public void delete(int clientId) {
        clienteCrudRepository.deleteById(clientId);
    }
}
