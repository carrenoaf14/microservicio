package com.prueba.microservicio.persistence.mapper;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.persistence.entity.Cuenta;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = MovementMapper.class)
public interface AccountMapper {

    @Mappings({
            @Mapping(source = "PKCuenta", target = "accountPK"),
            @Mapping(source = "saldoInicial", target = "initialBalance"),
            @Mapping(source = "numeroCuenta", target = "accountNumber"),
            @Mapping(source = "PKCliente", target = "clientPK"),
            @Mapping(source = "movimientos", target = "movements")
    })
    Account toAccount(Cuenta cuenta);
    List<Account> toAccounts(List<Cuenta> cuentas);

    @InheritInverseConfiguration
    @Mapping(target = "estado", ignore = true)
    @Mapping(target = "movimientos", ignore = true)
    @Mapping(target = "tipoCuenta", ignore = true)
    @Mapping(target = "cliente", ignore = true)
    Cuenta toCuenta(Account account);
}
