package com.prueba.microservicio.persistence.mapper;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.persistence.entity.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;


@Mapper(componentModel = "spring", uses = AccountMapper.class)
public interface ClientMapper {
    @Mappings({
            @Mapping(source = "nombre", target = "name"),
            @Mapping(source = "identificacion", target = "identification"),
            @Mapping(source = "estado", target = "state"),
            @Mapping(source = "pkCliente", target = "clientpk"),
            @Mapping(source = "cuentas", target = "accounts")
    })
    Client toClient(Cliente cliente);
    List<Client> toClients(List<Cliente> clientes);

    @InheritInverseConfiguration
    @Mapping(target = "direccion", ignore = true)
    @Mapping(target = "genero", ignore = true)
    @Mapping(target = "telefono", ignore = true)
    @Mapping(target = "contrasena", ignore = true)
    Cliente toCliente(Client client);
}







