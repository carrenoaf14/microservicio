package com.prueba.microservicio.persistence.mapper;

import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.persistence.entity.Movimiento;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovementMapper {
    @Mappings({
            @Mapping(source = "PKMovimiento",target = "movementPK"),
            @Mapping(source = "saldo",target = "balance"),
            @Mapping(source = "valor",target = "value"),
            @Mapping(source = "fecha",target = "date"),
            @Mapping(source = "tipoMovimiento", target = "movementType"),
            @Mapping(source = "PKCuenta", target = "accountPK")
    })
    Movement toMovement(Movimiento movimiento);
    List<Movement> toMovements(List<Movimiento> movimientos);

    @InheritInverseConfiguration
    @Mapping(target = "cuenta", ignore = true)
    Movimiento toMovimiento(Movement movement);

}
