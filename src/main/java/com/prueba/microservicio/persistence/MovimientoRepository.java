package com.prueba.microservicio.persistence;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.Movement;
import com.prueba.microservicio.domain.repository.MovementRepository;
import com.prueba.microservicio.persistence.crud.ClienteCrudRepository;
import com.prueba.microservicio.persistence.crud.MovimientoCrudRepository;
import com.prueba.microservicio.persistence.entity.Cliente;
import com.prueba.microservicio.persistence.entity.Movimiento;
import com.prueba.microservicio.persistence.mapper.ClientMapper;
import com.prueba.microservicio.persistence.mapper.MovementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public class MovimientoRepository implements MovementRepository {

    @Autowired
    private MovimientoCrudRepository movimientoCrudRepository;

    @Autowired
    private MovementMapper movementMapper;

    @Override
    public List<Movement> getAll(){
        List<Movimiento> movimientos = (List<Movimiento>) movimientoCrudRepository.findAll();
        return movementMapper.toMovements(movimientos);
    }

    @Override
    public Movement save(Movement movement) {
        Movimiento movimiento = movementMapper.toMovimiento(movement);
        return movementMapper.toMovement(movimientoCrudRepository.save(movimiento));
    }

    @Override
    public Optional<Movement> getMovement(int id) {
        return movimientoCrudRepository.findById(id).map(movimiento -> movementMapper.toMovement(movimiento));
    }

    @Override
    public void delete(int movementId) {
        movimientoCrudRepository.deleteById(movementId);
    }

    @Override
    public List<Movement> findByFechaBetween(LocalDateTime fechaInicio, LocalDateTime fechaFin){
        List<Movimiento> movimientos = movimientoCrudRepository.findByFechaBetween(fechaInicio,fechaFin);
        return movementMapper.toMovements(movimientos);
    }
}
