package com.prueba.microservicio.persistence.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "movimientos")
public class Movimiento {
        private BigDecimal saldo;
        private BigDecimal valor;
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        private LocalDateTime fecha;
        @Column(name = "pk_cuenta")
        private Integer PKCuenta;
        @Column(name = "tipo_movimiento")
        private String tipoMovimiento;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "pk_movimiento")
        private Integer PKMovimiento;

        @ManyToOne
        @JoinColumn(name = "pk_cuenta", insertable = false, updatable = false)
        private Cuenta cuenta;

}
