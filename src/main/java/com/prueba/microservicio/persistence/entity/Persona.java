package com.prueba.microservicio.persistence.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.math.BigInteger;

@Data
public class Persona {

    private String nombre;
    private String genero;
    private String direccion;
    private BigInteger telefono;
    private BigInteger identificacion;
}
