package com.prueba.microservicio.persistence.entity;

import javax.persistence.*;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;

@Data
@Entity
@Table(name = "clientes")
public class Cliente{

    private String nombre;
    private String genero;
    private String direccion;
    private BigInteger identificacion;
    private BigInteger telefono;
    private String contrasena;
    private boolean estado;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_cliente")
    private Integer pkCliente;

    @OneToMany(mappedBy = "cliente")
    private List<Cuenta> cuentas;

}
