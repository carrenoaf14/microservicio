package com.prueba.microservicio.persistence.entity;

import javax.persistence.*;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "cuentas")
@Data
public class Cuenta {
    private boolean estado;

    @Column(name = "tipo_cuenta")
    private String tipoCuenta;

    @Column(name = "saldo_inicial")
    private BigDecimal saldoInicial;

    @Column(name = "numero_cuenta")
    private BigInteger numeroCuenta;

    @Column(name = "pk_cliente")
    private Integer PKCliente;

    @Id
    @Column(name =  "pk_cuenta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer PKCuenta;

    @ManyToOne
    @JoinColumn(name = "pk_cliente", insertable = false, updatable = false)
    private Cliente cliente;

    @OneToMany(mappedBy = "cuenta",cascade =  {CascadeType.ALL})
    private List<Movimiento> movimientos;
}
