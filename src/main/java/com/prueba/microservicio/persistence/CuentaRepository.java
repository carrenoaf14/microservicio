package com.prueba.microservicio.persistence;

import com.prueba.microservicio.domain.Account;
import com.prueba.microservicio.domain.repository.AccountRepository;
import com.prueba.microservicio.persistence.crud.CuentaCrudRepository;
import com.prueba.microservicio.persistence.entity.Cuenta;
import com.prueba.microservicio.persistence.entity.Movimiento;
import com.prueba.microservicio.persistence.mapper.AccountMapper;
import com.prueba.microservicio.persistence.mapper.MovementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class CuentaRepository implements AccountRepository{

    @Autowired
    private CuentaCrudRepository cuentaCrudRepository;

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private AccountMapper accountMapper;


    @Override
    public List<Account> getAll(){
        List<Cuenta> accounts = (List<Cuenta>) cuentaCrudRepository.findAll();
        return accountMapper.toAccounts(accounts);
    }

    @Override
    public Account save(Account account) {
        Cuenta cuenta = accountMapper.toCuenta(account);
        return accountMapper.toAccount(cuentaCrudRepository.save(cuenta));
    }

    @Override
    public Optional<Account> getAccount(int id) {
        return cuentaCrudRepository.findById(id).map(acc -> accountMapper.toAccount(acc));
    }

    @Override
    public void delete(int accountId) {
        cuentaCrudRepository.deleteById(accountId);
    }

    @Override
    public List<Account> getByPKClientBetweenDate(int id, LocalDateTime startDate, LocalDateTime endDate){
        System.out.printf(startDate.toString() + endDate.toString());
        return accountMapper.toAccounts(cuentaCrudRepository.findByPKCliente(id)).stream().map(account -> {
            account.setMovements(movimientoRepository.findByFechaBetween(startDate,endDate));
            return account;
        }).collect(Collectors.toList());

    }
}
