package com.prueba.microservicio;

import com.prueba.microservicio.domain.Client;
import com.prueba.microservicio.domain.repository.ClientRepository;
import com.prueba.microservicio.domain.service.ClientService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;
    @InjectMocks
    private ClientService clientService;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAll() {
        Client client1 = new Client();
        client1.setClientpk(1);
        client1.setName("Andres");
        client1.setIdentification(new BigInteger("13"));

        Client client2 = new Client();
        client2.setClientpk(2);
        client2.setName("Felipe");
        client2.setIdentification(new BigInteger("12"));

        List<Client> clients = Arrays.asList(client1, client2);

        Mockito.when(clientRepository.getAll()).thenReturn(clients);

        List<Client> result = clientService.getAll();

        Assertions.assertThat(result).isEqualTo(clients);
    }

    @Test
    void save() {
        Client client = new Client();
        client.setName("Angel");
        client.setIdentification(new BigInteger("124"));

        Client savedClient = new Client();
        savedClient.setClientpk(1);
        savedClient.setName("Angel");
        savedClient.setIdentification(new BigInteger("124"));

        Mockito.when(clientRepository.save(client)).thenReturn(savedClient);

        Client result = clientService.save(client);

        Assertions.assertThat(result).isEqualTo(savedClient);
    }

    @Test
    void update() {
        Client client = new Client();
        client.setClientpk(1);
        client.setName("Monica");
        client.setIdentification(new BigInteger("1234"));

        Client updatedClient = new Client();
        updatedClient.setClientpk(1);
        updatedClient.setName("Sofia");
        updatedClient.setIdentification(new BigInteger("1234"));

        Mockito.when(clientRepository.getClient(1)).thenReturn(Optional.of(client));
        Mockito.when(clientRepository.save(updatedClient)).thenReturn(updatedClient);

        Client result = clientService.update(updatedClient);

        Assertions.assertThat(result).isEqualTo(updatedClient);
    }

    @Test
    void getClient() {
        Client client = new Client();
        client.setClientpk(1);
        client.setName("Marcelino");
        client.setIdentification(new BigInteger("1234"));

        Mockito.when(clientRepository.getClient(1)).thenReturn(Optional.of(client));

        Optional<Client> result = clientService.getClient(1);

        Assertions.assertThat(result).isEqualTo(Optional.of(client));
    }

    @Test
    void delete() {
        Mockito.when(clientRepository.getClient(1)).thenReturn(Optional.of(new Client()));

        boolean result = clientService.delete(1);

        Assertions.assertThat(result).isTrue();
    }
}
